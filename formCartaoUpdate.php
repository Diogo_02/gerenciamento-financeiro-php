<?php require_once("header.php"); ?>
<div class='home'>
		<div class='home_background'></div>
		<div class='background_image background_city' style='background-image:url(images/city_5.png)'></div>
		<div class='cloud cloud_1'><img src='images/cloud_left.png' alt=''></div>
		<div class='cloud cloud_2'><img src='images/cloud_left.png' alt=''></div>
		<div class='cloud cloud_3'><img src='images/cloud_left_full.png' alt=''></div>
		<div class='cloud cloud_4'><img src='images/cloud_left.png' alt=''></div>
		<div class='home_container'>
			<div class='container'>
				<div class='row'>
					<div class='col'>
						<div class='home_content'>
							<div class='home_title'>Cartões</div>
							<div class='breadcrumbs'>
								<ul class='d-flex flex-row align-items-center justify-content-start'>
									<li><a href='index.php'>Home</a></li>
									<li>Cartões</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class='contact'>
		<div class='container'>
            <div class='col-lg-8 contact_col'>
                <h3>Atualizar</h3>
<?php
    echo"
    <form id='message_form' class='message_form' action='ExecucaoCartao.php' enctype='multipart/form-data' method='POST'>
        <input type='hidden' class='message_input' name='nc' value='{$_GET['numeroCartao']}' /></br>
        <input type='text' class='message_input' name='prazo' placeholder='Dia de vencimento' /></br>
        <input type='text' class='message_input' name='saldo' placeholder='Saldo' /></br>
        <label>Tipo do Cartão</label></br>
        <select name='tipoCartao' class='form-control' required>
            <option selected disabled>Selecione o Tipo</option>
            <option value='1'> Débito</option>
            <option value='0'> Crédito </option>
        </select>
        <br>   
        <label>Nome do banco</label></br>
        <select name='nomeBanco' class='form-control' required>
            <option selected disabled>Selecione o Tipo</option>
            <option value='Bradesco'> Bradesco</option>
            <option value='Banco do Brasil'> Banco do Brasil </option>
            <option value='Santander'> Santander </option>
            <option value='Visa'> Visa </option>
            <option value='Itau'> Itau </option>
        </select><br>
        <input type='submit' name='form' value='Atualizar' class='message_form_button'/>
    </form>
    ";
?>
        </div>
    </div>
</div>
<?php require_once("rodape.php"); ?>