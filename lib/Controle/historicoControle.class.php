<?php
require_once("lib/Controle/Conexao.class.php");
require_once("lib/Modelo/HistoricoModelo.class.php");
final class HistoricoControle{
    public function consultaEntrada($cpf){
        $conexao = new Conexao("lib/Controle/mysql.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM historico WHERE valorE != 0 and id_conta='$cpf';");
        $comando->execute();
        $resultado = $comando->fetchAll();
        $lista = [];
        foreach($resultado as $item){
            $cartao = new HistoricoModelo();
            $cartao->setIdHistorico($item->id_history);
            $cartao->setIdConta($item->id_conta);
            $cartao->setNomeRegistro($item->nome);
            $cartao->setData($item->data);
            $cartao->setValorE($item->valorE);
            array_push($lista, $cartao);
        }
        $conexao->__destruct();
        return $lista;
    }
    public function consultaSaida($cpf){
        $conexao = new Conexao("lib/Controle/mysql.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM historico WHERE valorS != 0 and id_conta = '$cpf';");
        $comando->execute();
        $resultado = $comando->fetchAll();
        $lista = [];
        foreach($resultado as $item){
            $cartao = new HistoricoModelo();
            $cartao->setIdHistorico($item->id_history);
            $cartao->setIdCartao($item->id_card);
            $cartao->setNomeRegistro($item->nome);
            $cartao->setData($item->data);
            $cartao->setValorS($item->valorS);
            array_push($lista, $cartao);
        }
        $conexao->__destruct();
        return $lista;
    }
    public function inserirRegistro($registro){
        $conexao = new Conexao("lib/Controle/mysql.ini");
        $sql = "INSERT INTO historico(id_conta,nome,data,valorE,valorS) VALUES(:ic,:no,:da,:ve,:vs);";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue(":ic", $registro->getIdConta());
        //var_dump($registro->getIdCartao());
        $comando->bindValue(":no", $registro->getNomeRegistro());
        $comando->bindValue(":da", $registro->getData());
        $comando->bindValue(":ve", $registro->getValorE());
        $comando->bindValue(":vs", $registro->getValorS());
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
    public function balanco($cpf){
        $conexao = new Conexao("lib/Controle/mysql.ini");
        $sqlE ="SELECT SUM(valorS)-SUM(valorE) as valor1 FROM historico WHERE id_conta ='$cpf';";
        //$sqlS ="SELECT SUM(valorS) as valor2 FROM historico;";
        $comandoE = $conexao->getConexao()->prepare($sqlE);
        //$comandoS = $conexao->getConexao()->prepare($sqlS);
        if($comandoE->execute()){
            while ($resultado = $comandoE->fetchAll()){;
                    $a =$resultado[0]->valor1;
                if ($a < 0 ) {
                    echo"<p>Você está em dívida: $a</p>";
                } else {
                    echo"<p>Você está em lucro: $a</p>";
                }
            }
            $conexao->__destruct();
        }
    }
}
?>